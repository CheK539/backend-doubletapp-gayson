# Запуск
Команда запуска бота:  
`python manage.py start_bot`  

Токен для бота хранится по пути:  
`/src/config/bot_token.py`  

Ендпоинт выдачи информации о пользователе по telegram_id:  
`/api/me/{telegram_id}`
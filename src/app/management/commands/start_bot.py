from django.core.management import BaseCommand
from app.internal.transport.bot.handlers import TelegramBot


class Command(BaseCommand):
    def handle(self, *args, **options):
        TelegramBot().run()

from django.contrib import admin

from app.internal.admin.telegram_user import TelegramUserAdmin

admin.site.site_title = "Backend course"
admin.site.site_header = "Backend course"

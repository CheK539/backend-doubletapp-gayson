from django.urls import path

from app.internal.transport.rest import handlers

urlpatterns = [
    path('me/<int:telegram_id>/', handlers.TelegramUserView.as_view(), name='me')
]

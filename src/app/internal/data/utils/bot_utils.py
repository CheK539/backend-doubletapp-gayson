from telegram import KeyboardButton, ReplyKeyboardMarkup


def get_keyboard_markup(buttons: [KeyboardButton], one_time_keyboard: bool = False) -> ReplyKeyboardMarkup:
    return ReplyKeyboardMarkup.from_column(buttons, one_time_keyboard=one_time_keyboard)
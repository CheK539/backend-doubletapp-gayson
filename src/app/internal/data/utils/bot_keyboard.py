from telegram import KeyboardButton

from app.internal.data.enums.commands import Commands


class BotKeyboard:
    phone_button = KeyboardButton(text=Commands.SET_PHONE.value, request_contact=True)
    me_button = KeyboardButton(text=Commands.ME.value)
from enum import Enum


class Commands(Enum):
    START = '/start'
    ME = '/me'
    SET_PHONE = '/set_phone'
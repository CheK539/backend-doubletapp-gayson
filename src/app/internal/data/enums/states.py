from enum import Enum


class States(Enum):
    Register = 0
    Finish = 1

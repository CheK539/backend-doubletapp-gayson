from app.internal.models.telegram_user import TelegramUser


class TelegramUserRepository:
    telegram_user_dao = TelegramUser.objects

    def create(self, telegram_id: int) -> TelegramUser:
        return self.telegram_user_dao.get_or_create(telegram_id=telegram_id)

    def get_by_telegram_id(self, telegram_id: int) -> TelegramUser:
        return self.telegram_user_dao.get(telegram_id=telegram_id)

    def update(self, telegram_user: TelegramUser):
        telegram_user.save()

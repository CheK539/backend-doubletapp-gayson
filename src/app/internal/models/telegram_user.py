from django.db import models
from django.core import serializers


class TelegramUser(models.Model):
    telegram_id = models.BigIntegerField()
    phone = models.CharField(max_length=20, null=True)

    def to_json(self):
        return serializers.serialize('json', [self])

    def __str__(self):
        return f'TelegramId: {self.telegram_id}'

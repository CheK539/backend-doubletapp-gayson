from django.http import JsonResponse, HttpResponse
from django.views import View

from app.internal.data.repositories.telegram_user_repository import TelegramUserRepository


class TelegramUserView(View):

    telegram_user_repository = TelegramUserRepository()

    def get(self, request, telegram_id):
        telegram_user = self.telegram_user_repository.get_by_telegram_id(telegram_id)

        return HttpResponse(telegram_user.to_json())

from app.internal.data.enums.commands import Commands
from app.internal.data.enums.states import States
from app.internal.data.repositories.telegram_user_repository import TelegramUserRepository
from telegram import Update
from telegram.ext import Updater, CallbackContext, ConversationHandler, MessageHandler, Filters
from config.bot_token import BOT_TOKEN
from app.internal.data.utils.bot_keyboard import BotKeyboard
from app.internal.data.utils.bot_utils import get_keyboard_markup


class TelegramBot:

    telegram_user_repository = TelegramUserRepository()

    def __start_command(self, update: Update, context: CallbackContext):
        reply_markup = get_keyboard_markup([BotKeyboard.phone_button], True)

        user_id = update.effective_user.id
        self.telegram_user_repository.create(user_id)

        context.bot.send_message(chat_id=update.effective_message.chat_id, text='Send your phone number',
                                 reply_markup=reply_markup)

        return States.Register

    def __set_phone_command(self, update: Update, context: CallbackContext):
        phone = update.effective_message.contact.phone_number
        user_id = update.effective_user.id

        telegram_user = self.telegram_user_repository.get_by_telegram_id(user_id)
        telegram_user.phone = phone

        self.telegram_user_repository.update(telegram_user)

        reply_markup = get_keyboard_markup([BotKeyboard.phone_button, BotKeyboard.me_button])
        context.bot.send_message(chat_id=update.effective_message.chat_id, text='Phone number saved',
                                 reply_markup=reply_markup)

        return States.Finish

    def __get_me_command(self, update: Update, context: CallbackContext):
        user_id = update.effective_user.id
        telegram_user = self.telegram_user_repository.get_by_telegram_id(user_id)

        context.bot.send_message(update.effective_message.chat_id, telegram_user.to_json())

    def run(self):
        print('Bot started')

        bot = Updater(token=BOT_TOKEN)
        dispatcher = bot.dispatcher

        handler = ConversationHandler(
            entry_points=[MessageHandler(Filters.text(Commands.START.value), self.__start_command)],
            states={
                States.Register: [MessageHandler(Filters.contact, self.__set_phone_command)],
                States.Finish: [MessageHandler(Filters.contact, self.__set_phone_command),
                                MessageHandler(Filters.text(Commands.ME.value), self.__get_me_command)]
            },
            fallbacks=[]
        )
        dispatcher.add_handler(handler)

        bot.start_polling()
        bot.idle()

        print('Bot finished')
